<h2>SSPA DEMO</h2>

<p>A demo project to demonstrate on single-spa way of creating microfrontends.</p>
<ul>
<b>This project has three submodules :</b>
<li>sspa-app1 : A react microfrontend module</li>
<li>sspa-app2 : A vue microfrontend module</li>
<li>sspa-root : Single-spa root config</li>
</ul>

<p><b>Functionality:</b> Vue app modifies the counter and the react app displays it.</p>

<ul>
<b>Steps to clone the app along with the submodules :</b>
<li>git clone https://gitlab.com/mayankav/sspa-demo.git</li>
<li>Navigate to the cloned folder <b>sspa-demo</b></li>
<li>git submodule init</li>
<li>git submodule update</li>
</ul>

<ul>
<b>Steps to run the app :</b>
<li>navigate to sspa-app1 > npm install > npm start</li>
<li>navigate to sspa-app2 > npm install > npm run serve</li>
<li>navigate to sspa-root > npm install > npm start</li>
</ul>

<b>The application should be up on -</b> http://localhost:9000/<br>
<b>To run sub apps in standalone mode, check-</b> https://github.com/single-spa/standalone-single-spa-webpack-plugin

